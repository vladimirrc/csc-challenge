
module.exports = function sortCategoriesForInsert(inputJson) {

  // replace trailing commas from input
  var formattedJson = inputJson.replace(/\,(?!\s*[\{\"\w])/g, '');
  let properJsonOutput = JSON.parse(formattedJson);
  var categoryMapping = new Object();
  let linkedListCategories = new LinkedList();
  properJsonOutput.forEach(element => {
    let newElement = new ListNode(element);
    if (element.parent_id == null) {

      linkedListCategories.Add(newElement)
      categoryMapping[element.id] = newElement;
    }
    else {
      if (!(element.parent_id in categoryMapping)) {
        linkedListCategories.Add(newElement);
        categoryMapping[element.id] = newElement;
      }
      else {
        let parentNode = categoryMapping[element.parent_id];
        newElement.next = parentNode.next;
        if (parentNode.next != null) {
          parentNode.next.previous = newElement;
        }
        parentNode.next = newElement;
        categoryMapping[element.id]=newElement;
      }
    }
  }
  );
  return JSON.stringify(process(linkedListCategories.head))
}

// Converts the linkedlist to a simple array.
const process = obj => (({ data, next }) =>
  [data, ...((next !== null) ? process(next) : [])])(obj);

// Linked list support
class ListNode {
  constructor(data) {
    this.data = data
    this.next = null
    this.previous = null
  }
}

class LinkedList {
  constructor(head = null) {
    this.head = head
  }

  Add(newNode) {
    if (this.head == null) {
      this.head = newNode;
    }
    else {
      this.head.previous = newNode;
      newNode.next = this.head;
      this.head = newNode;
    }
  }

}